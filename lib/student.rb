class Student
  attr_reader :first_name, :last_name

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def courses
    @courses
  end

  def enroll(course)
    if @courses.any? {|c| c.conflicts_with?(course)}
      raise Exception
    end

    @courses << course unless course.students.include?(self)
    #Could have also used:       unless @courses.include?(course)
    #If I wouldn't have had access to students
    course.students << self
  end

  def course_load
    hash = Hash.new(0)
    @courses.each do |course|
      hash[course.department] += course.credits
    end
    hash
  end

end
